import re
import pandas as pd
from pyzotero import zotero
from collections import Counter


def load_markdown_file(path):
    # open text file in read mode
    text_file = open(path, 'r')

    # read whole file to a string
    content = text_file.read()

    # close file
    text_file.close()

    return content


def extract_citations(content):
    #regex_pattern = "@([^\s\];\]]+)[\s\];\]]"
    regex_pattern = "@([^\s\];,.\<]+)[\s\];\],.\<]"
    regex_pattern = '@([^\s\];,.\<{]+)[\s\];\],.\<]'
    p = re.compile(regex_pattern)

    citations = re.findall(p, content)
    return citations


def analyse_citations_count(citations_list):
    return Counter(citations_list)


def search_zotero(bibtex_keys_to_find):
    zot = zotero.Zotero('9840986', 'user', 'XKEjEmdhLzk5BofxWWl13aFH')
    #collection_list = {i['key'] for i in zot.all_collections()}
    all_zotero_items = zot.everything(zot.items())

    items_to_return = []

    for item in all_zotero_items:
        if 'extra' in item['data']:
            for bibtex_key in bibtex_keys_to_find:
                if bibtex_key in item['data']['extra']:
                    items_to_return.append({'bibtex_key': bibtex_key, 'zotero_item': item})

    return items_to_return


def load_journal_ranking(path):
    # This file is obtained by downloading it from https://www.scimagojr.com/journalrank.php
    df = pd.read_csv(path, sep=';')
    return df


def lookup_journal_ranking(df, issn):
    # Note to self: the Issn column contains a list of ISSNs as a string ...
    result = df.loc[df['Issn'].str.contains(issn)]
    return result.to_dict('records')


# First we load our manuscript
data = load_markdown_file('/Users/dennisverslegers/Documents/Academic/Research/Lit Rev/Drafts/0.6/Compiled.md')

# Next we extract the citations inside the manuscript
citations = extract_citations(data)

# Now it is time to find the unique citations and their count
citation_analysis = analyse_citations_count(citations)

# Let's get additional information from Zotero on these papers
zotero_results = search_zotero(set(citations))

# Let's put things together ...
papers = []
journal_df = load_journal_ranking()

for key, value in citation_analysis.items():

    paper = {
        'bibtex_key': key,
        'number_of_citations': value,
        'tags': None,
        'itemType': None,
        'issn': None,
        'publication_date': None,
        'journal_name': None,
        'journal_h_index': None,
        'journal_quartile': None,
    }

    # Let's see if we can find a zotero item to enrich the dataset
    zotero_items = [item['zotero_item'] for item in zotero_results if item['bibtex_key'] == key]

    if len(zotero_items):
        zotero_item = zotero_items[0]
        paper['tags'] = zotero_item['data']['tags']
        paper['itemType'] = zotero_item['data']['itemType']
        paper['publication_date'] = zotero_item['data']['date']

        if zotero_item['data']['itemType'] == 'journalArticle':
            paper['issn'] = zotero_item['data']['ISSN']
            if ',' in paper['issn']:
                paper['issn'] = zotero_item['data']['ISSN'].split(',')[0]
            paper['journal_name'] = zotero_item['data']['publicationTitle']

            # Now we should enrich our dataset with the journal ranking information
            if len(paper['issn']) > 4:
                # We should avoid lookups for empty ISSNs as this yields very strange results
                journal_details = lookup_journal_ranking(journal_df, paper['issn'].replace('-', ''))
                if len(journal_details):
                    paper['journal_name'] = journal_details[0]['Title']
                    paper['journal_h_index'] = journal_details[0]['H index']
                    paper['journal_quartile'] = journal_details[0]['SJR Best Quartile']
                    paper['journal_areas'] = journal_details[0]['Areas']

    papers.append(paper)

# Now we can place them inside a dataframe and export as a csv
citations_df = pd.DataFrame(papers)
citations_df.to_csv('citation_analysis.csv')

# We can do some additional analysis on the dataset
# Q1: How are the papers distributed over the journals?

