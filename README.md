This jupyter notebook provides the necessary functions to: 
* Extract bibtex citations from a markdown document
* Lookup citation details in a provided Betterbibtex JSON file
* Lookup journal details including journal ranking based on the scimagojr index of 2021

The results can be subsequently analysed in a tool of your choice (Excel, Tableau, ...).

Installation should be as simple as
```bash
$ cd /path/to/citation_analysis
$ poetry install
```
and subsequently launching the jupyter notebook
```bash
poetry run jupyter notebook
```

At this point you should be able to browse to http://localhost:8888 to use the notebook.